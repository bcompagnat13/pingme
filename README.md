# PingMe

PingMe is an instant messaging Java application using sockets.


## Limitations

- The server is configured to listen on port 9000 only.

- Username entered for connections to the server should be unique.

## Building and executing a custom runtime image

### Client

1. Execute `./gradlew :client:jlinkZip`.
2. You'll then find an archive folder under `./client/build/image-zip`
3. Extract the archive and run the application under `./bin/Pingme-Client`

### Server

1. Execute `./gradlew :server:jlinkZip`.
2. You'll then find an archive folder under `./server/build/image-zip`
3. Extract the archive and run the application under `./bin/Pingme-Server`

`