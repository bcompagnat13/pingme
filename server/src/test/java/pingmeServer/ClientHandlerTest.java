package pingmeServer;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import static org.mockito.Mockito.*;

public class ClientHandlerTest {

    private ClientHandler client1Handler;
    private ClientHandler client2Handler;
    private Server mockedServer;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    ObjectOutputStream client1OutputStream;
    ObjectOutputStream client2OutputStream;
    private User user1;
    private User user2;



    @BeforeEach
    public void beforeEach() throws IOException {
        user1 = new User("Testy1");
        user2 = new User("Testy2");

        mockedServer = mock(Server.class);
        serverSocket = new ServerSocket(9100);
        Socket sock = new Socket("127.0.0.1", 9100);
        clientSocket = serverSocket.accept();
        client1OutputStream = new ObjectOutputStream(sock.getOutputStream());

        client1Handler = new ClientHandler(new ObjectInputStream(clientSocket.getInputStream()),
                new ObjectOutputStream(clientSocket.getOutputStream()), mockedServer);
        new Thread(client1Handler).start();

        Socket sock2 = new Socket("127.0.0.1", 9100);
        Socket clientSocket2 = serverSocket.accept();
        client2OutputStream = new ObjectOutputStream(sock2.getOutputStream());
        client2Handler = new ClientHandler(new ObjectInputStream(clientSocket2.getInputStream()),
                new ObjectOutputStream(clientSocket2.getOutputStream()), mockedServer);
        new Thread(client2Handler).start();
    }

    @Test
    public void updateClientUserList_test() throws IOException {
        client1OutputStream.writeObject(user1);
        client2OutputStream.writeObject(user2);

        Mockito.verify(mockedServer, Mockito.times(1)).addUser(user1, client1Handler);
        Mockito.verify(mockedServer, Mockito.times(1)).addUser(user2, client2Handler);
    }
}