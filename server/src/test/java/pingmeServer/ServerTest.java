package pingmeServer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pingmeUtil.models.Conversation;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.util.ArrayList;
import java.util.TreeSet;

import static org.mockito.Mockito.*;

public class ServerTest {
    private Server server;
    private ClientHandler mockedClient1Handler;
    private ClientHandler mockedClient2Handler;
    private User user1;
    private User user2;

    @BeforeEach
    public void beforeEach() {
        server = new Server();
        mockedClient1Handler = mock(ClientHandler.class);
        mockedClient2Handler = mock(ClientHandler.class);
        user1 = new User("Testy1");
        user2 = new User("Testy2");
    }

    @Test
    public void addUser_test() {
        server.addUser(user1, mockedClient1Handler);
        server.addUser(user2, mockedClient2Handler);
        TreeSet<User> userSet = server.getClientUserSet();
        Assertions.assertTrue(userSet.contains(user1));
        Assertions.assertTrue(userSet.contains(user2));
        Assertions.assertEquals(2, userSet.size());


        server.addUser(user1, mockedClient1Handler);
        userSet = server.getClientUserSet();
        Assertions.assertTrue(userSet.contains(user1));
        Assertions.assertTrue(userSet.contains(user2));
        Assertions.assertEquals(2, userSet.size());

        Mockito.verify(mockedClient1Handler, Mockito.times(2)).updateClientUserList(userSet);
        Mockito.verify(mockedClient2Handler, Mockito.times(2)).updateClientUserList(userSet);
    }

    @Test
    public void removeHandler_test() {
        server.addUser(user1, mockedClient1Handler);
        TreeSet<User> onlyUser1Set = server.getClientUserSet();
        Assertions.assertTrue(onlyUser1Set.contains(user1));
        Assertions.assertEquals(1, onlyUser1Set.size());

        server.addUser(user2, mockedClient2Handler);
        TreeSet<User> bothUserSet = server.getClientUserSet();
        Assertions.assertTrue(bothUserSet.contains(user1));
        Assertions.assertTrue(bothUserSet.contains(user2));
        Assertions.assertEquals(2, bothUserSet.size());


        server.removeHandler(user2);
        bothUserSet = server.getClientUserSet();
        Assertions.assertTrue(bothUserSet.contains(user1));
        Assertions.assertTrue(bothUserSet.contains(user2));
        Assertions.assertEquals(2, bothUserSet.size());
        Mockito.verify(mockedClient1Handler, Mockito.times(1)).updateClientUserList(onlyUser1Set);
        Mockito.verify(mockedClient1Handler, Mockito.times(2)).updateClientUserList(bothUserSet);
        Mockito.verify(mockedClient2Handler, Mockito.times(1)).updateClientUserList(bothUserSet);
        Mockito.verify(mockedClient2Handler, never()).updateClientUserList(onlyUser1Set);
    }

    @Test
    public void addMessage_test() {
        Message firstMessage = new Message("Test", user1, user2);
        Message secondMessage = new Message("Test2", user2, user1);
        User user3 = new User("Testy3");
        ClientHandler mockedClient3Handler = mock(ClientHandler.class);

        server.addUser(user1, mockedClient1Handler);
        server.addUser(user2, mockedClient2Handler);
        server.addUser(user3, mockedClient3Handler);
        server.addMessage(firstMessage);
        server.addMessage(secondMessage);

        Mockito.verify(mockedClient1Handler, Mockito.times(1)).updateConversation(firstMessage);
        Mockito.verify(mockedClient2Handler, Mockito.times(1)).updateConversation(firstMessage);
        Mockito.verify(mockedClient1Handler, Mockito.times(1)).updateConversation(secondMessage);
        Mockito.verify(mockedClient2Handler, Mockito.times(1)).updateConversation(secondMessage);
        Mockito.verify(mockedClient3Handler, never()).updateConversation(firstMessage);
        Mockito.verify(mockedClient3Handler, never()).updateConversation(secondMessage);
    }

    @Test
    void findConversation_test() {
        User user3 = new User("Testy3");
        ClientHandler mockedClient3Handler = mock(ClientHandler.class);
        Message firstMessage = new Message("Test", user1, user2);
        Message secondMessage = new Message("Test2", user2, user1);
        Message thirdMessage = new Message("Test3", user3, user1);

        server.addUser(user1, mockedClient1Handler);
        server.addUser(user2, mockedClient2Handler);
        server.addUser(user3, mockedClient3Handler);
        server.addMessage(firstMessage);
        server.addMessage(secondMessage);
        server.addMessage(thirdMessage);

        ArrayList<Conversation> conversations = server.findConversations(user1);
        Assertions.assertEquals(2, conversations.size());
        Assertions.assertTrue(conversations.get(0).getParticipant1().equals(user2) ||
                conversations.get(0).getParticipant2().equals(user2));
        Assertions.assertTrue(conversations.get(1).getParticipant1().equals(user3) ||
                conversations.get(0).getParticipant2().equals(user3));
    }
}