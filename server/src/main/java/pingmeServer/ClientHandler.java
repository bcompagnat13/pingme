package pingmeServer;

import pingmeUtil.models.Conversation;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Set;

/**
 * Class to handle each new connection to the server as its own thread.
 */
public class ClientHandler implements Runnable {
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private Server server;
    private User clientUser;

    /**
     * Constructor of the ClientHandler.
     * @param input ObjectInputStream associated to the client socket.
     * @param output ObjectOutputStream associated to the client socket.
     * @param server Server.
     */
    public ClientHandler(ObjectInputStream input, ObjectOutputStream output, Server server) {
        this.server = server;
        try {
           this.input = input;
           this.output = output;
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    /**
     * Implementation of the run method of the Runnable interface.
     * Listens to the ObjectInputStream.
     */
    public void run() {
        Object objectRecieved;
        try {
            while ((objectRecieved = input.readObject()) != null) {
                if(objectRecieved instanceof User) {
                    this.process((User) objectRecieved);
                } else if (objectRecieved instanceof Message) {
                   this.process((Message) objectRecieved);
                }
            }
        } catch (Exception ex) {
            if (this.clientUser != null)
                System.out.println("Client disconnected : " + this.clientUser);
            else
                System.out.println("Client disconnected");
            this.clientUser.setIsOnline(false); //TODO : doesn't seem to work..
            this.server.removeHandler(this.clientUser);
        }
    }

    /**
     * Sends the set of User to the client.
     * @param users Set of user to distribute.
     */
    public void updateClientUserList(Set<User> users) {
        try {
            output.reset(); // Needed to override last Set of User.
            output.writeObject(users);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Sends a Message object to the client for theirs to update the conversation.
     * @param newMessage Message to distribute.
     */
    public void updateConversation(Message newMessage) {
        try {
            output.writeObject(newMessage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Process a new Message received.
     * Sends to the server, so it can update both participants.
     * @param messageReceived Message received to process.
     */
    private void process(Message messageReceived) {
        System.out.println(this.clientUser + " received a message.");
        System.out.println("From : " + messageReceived.getIsFrom());
        System.out.println("To : " + messageReceived.getIsTo());
        System.out.println("Message : " + messageReceived.getMessage());
        this.server.addMessage(messageReceived);
    }

    /**
     * Process a new User received.
     * Sends to the server, so it can update all clients.
     * Calls UpdateConversations.
     * @param userReceived User received to process.
     */
    private void process(User userReceived) {
        this.clientUser = userReceived;
        this.clientUser.setIsOnline(true);
        this.server.addUser(this.clientUser, this);
        this.updateConversations();
    }

    /**
     * Update the client for each conversation the server has related to this user.
     */
    private void updateConversations() {
        ArrayList<Conversation> conversations = this.server.findConversations(this.clientUser);
        if(conversations.isEmpty()) {
           return;
        }
        try {
            for(Conversation conversation : conversations) {
                output.writeObject(conversation);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
