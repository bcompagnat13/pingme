package pingmeServer;

import static pingmeUtil.Constants.*;

import pingmeUtil.models.Conversation;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Main class for Pingme Server
 */
public class Server {
    private TreeMap<User, ClientHandler> clientMap;
    private ArrayList<Conversation> conversationList;

    /**
     * Protected constructor so its instanced only in the main or tests.
     */
    protected Server() {
        clientMap = new TreeMap<>();
        conversationList = new ArrayList<>();
    }

    /**
     * Main for starting the server.
     * @param args to starts the application.
     */
    public static void main(String[] args) {
        new Server().go();
    }

    /**
     * Opening the socket and wait for new connections to initiate the service.
     * Creates a new Thread with ClientHandler class for each connection.
     */
    private void go() {
        try {
            ServerSocket serverSock = new ServerSocket(SERVER_PORT);
            System.out.println("Listening on port : " + SERVER_PORT);
            while(true) {
                Socket clientSocket = serverSock.accept();
                new Thread(new ClientHandler(
                        new ObjectInputStream(clientSocket.getInputStream()),
                        new ObjectOutputStream(clientSocket.getOutputStream()), this)).start();
                System.out.println("Got a new connection");
            }
        } catch (Exception ex) {
            // TODO : Catch more exceptions and handle in different way. We could restart the server as well.
            ex.printStackTrace();
        }
    }

    /**
     * Add a new or, an old user logging back, to our list of users linked to their ClientHandler.
     * On each addition, calls updateClientsUserSet().
     * @param user logging in.
     * @param clientHandler that manages this specific user.
     */
    public void addUser(User user, ClientHandler clientHandler) {
        synchronized (this) {
            if (this.clientMap.containsKey(user)) {
                System.out.println("Adding an old user : " + user);
                this.clientMap.remove(user);
                this.clientMap.put(user, clientHandler);
            } else {
                System.out.println("Adding a new user : " + user);
                this.clientMap.put(user, clientHandler);
            }
        }
        updateClientsUserSet();
    }

    /**
     * Removing ClientHandler associated with a user when he logs off.
     * Calls updateClientsUserSet().
     * @param user to remove the handler.
     */
    public void removeHandler(User user) {
        synchronized (this) {
            this.clientMap.remove(user);
            this.clientMap.put(user, null);
        }
        updateClientsUserSet();
    }

    /**
     * Adding a new message to the appropriate conversation.
     * Creates a conversation if it doesn't exist yet.
     * Calls updateConversation for both ClientHandler.
     * @param message received to add to the conversation.
     */
    public void addMessage(Message message) {
        System.out.println("Adding Message to conversation");
        Conversation conversation = findConversation(message.getIsFrom(), message.getIsTo());
        if(conversation == null) {
            System.out.println("Creating a conversation to add the Message");
            conversation = new Conversation(message.getIsFrom(), message.getIsTo());
            synchronized (this) {
                conversationList.add(conversation);
            }
        }
        synchronized (this) {
            conversation.addMessage(message);
        }
        clientMap.get(message.getIsTo()).updateConversation(message);
        clientMap.get(message.getIsFrom()).updateConversation(message);
    }

    /**
     * Finds all conversation from the conversationList related to the user.
     * @param participant User to find conversations about.
     * @return List of conversation the user is involved in.
     */
    public ArrayList<Conversation> findConversations(User participant) {
        ArrayList<Conversation> relevantConversations = new ArrayList<>();
        for(Conversation conversation : this.conversationList) {
            if(conversation.getParticipant1().equals(participant) || conversation.getParticipant2().equals(participant)) {
                relevantConversations.add(conversation);
            }
        }
        return relevantConversations;
    }

    /**
     * Getter for a set of the users registered.
     * @return Set of User.
     */
    public TreeSet<User> getClientUserSet() {
        return new TreeSet<>(clientMap.keySet());
    }

    /**
     * Finds the conversation involving two users.
     * @param participant1 User participating in the desired conversation.
     * @param participant2 Other User participating in the desired conversation.
     * @return Conversation if found. Null when no conversation exists yet for both users.
     */
    private Conversation findConversation(User participant1, User participant2) {
        for(Conversation conversation : this.conversationList) {
            if((conversation.getParticipant1().equals(participant1) && conversation.getParticipant2().equals(participant2)) ||
            (conversation.getParticipant2().equals(participant1) && conversation.getParticipant1().equals(participant2))) {
                return conversation;
            }
        }
        return null;
    }

    /**
     * Calls updateClientUserList for each registered ClientHandlers to update clients list of users.
     */
    private void updateClientsUserSet() {
        for(ClientHandler clientHandler : clientMap.values()) {
            if(clientHandler != null) {
                clientHandler.updateClientUserList(getClientUserSet());
            }
        }
    }
}
