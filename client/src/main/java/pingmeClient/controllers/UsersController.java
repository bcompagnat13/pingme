package pingmeClient.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pingmeUtil.models.User;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * UserView Controller.
 */
public class UsersController implements Initializable {

    @FXML
    public ListView<User> userListView;

    private final User myUser;
    private final ClientController clientController;
    private final ObservableList<User> userList = FXCollections.observableArrayList();

    /**
     * Constructor.
     * @param myUser Client's User.
     * @param clientController ClientController.
     */
    UsersController(User myUser, ClientController clientController) {
        this.clientController = clientController;
        this.myUser = myUser;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.userListView.setItems(userList);
        this.userListView.setCellFactory(list -> new UserListCell());
        this.userListView.setOnMouseClicked(new MouseClick());
    }

    /**
     * Handler to replace the user set with a new one.
     * @param users Set of users received by the server.
     */
    public void addUsers(Set<User> users) {
        users.remove(this.myUser);
        Platform.runLater(() -> {
            userList.clear();
            userList.addAll(users);
        });
    }

    /**
     * Class to properly format the users displayed.
     */
    private static class UserListCell extends ListCell<User> {
        @Override
        protected void updateItem(User user, boolean empty) {
            super.updateItem(user, empty);
            if (user == null) {
                setText(null);
                return;
            }

            Color textColor;
            String text = user.toString();

            if (user.isOnline()) {
                textColor = Color.GREEN;
            } else {
                textColor = Color.BLACK;
            }
            setTextFill(textColor);
            setText(text);
        }
    }

    /**
     * Class to handle user selection.
     */
    private class MouseClick implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent click) {
            User selectedUser = userListView.getSelectionModel().getSelectedItem();
            clientController.selectUser(selectedUser);
        }
    }
}
