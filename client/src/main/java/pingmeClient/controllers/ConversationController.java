package pingmeClient.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import pingmeUtil.models.Conversation;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the ConversationView.
 */
public class ConversationController implements Initializable {

    @FXML
    public Label userLabel;
    public Button sendButton;
    public TextField messageTextField;
    public AnchorPane conversationWindowAnchorPane;
    public ListView<Message> conversationListView;

    private User myUser;
    private User participant;
    private ClientController clientController;
    private Conversation currentConversation;

    /**
     * Constructor.
     * @param myUser Client's user.
     * @param clientController ClientController.
     */
    public ConversationController(User myUser, ClientController clientController) {
        this.myUser = myUser;
        this.clientController = clientController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        userLabel.setText("Select a user");
        this.sendButton.setOnAction(this::sendButtonAction);
        setEnterKeyAction(conversationWindowAnchorPane);
        this.conversationListView.setCellFactory(list -> new ConversationCell());
    }

    /**
     * Handler to display a conversation.
     * @param currentConversation Conversation desired to display.
     */
    public void changeConversation(Conversation currentConversation) {
        this.participant = currentConversation.getParticipant2();
        this.currentConversation = currentConversation;
        userLabel.setText(this.participant.getUsername());
        this.conversationListView.setItems(currentConversation.getMessages());
    }

    /**
     * Getter for the current displayed conversation.
     * @return current displayed Conversation.
     */
    public Conversation getCurrentConversation() {
        return currentConversation;
    }

    /**
     * Send button handler to send the message to the server.
     * @param event ActionEvent of the button pressed.
     */
    private void sendButtonAction(ActionEvent event) {
        // TODO input validation
        System.out.println("Sending message to the server for : " + this.participant);
        Message message = new Message(messageTextField.getText(), this.myUser, this.participant);
        this.clientController.sendMessage(message);
        this.messageTextField.setText("");
    }

    /**
     * Enter key handler to send a message from the keyboard when pressed.
     * @param root AnchorPane to associate the action to.
     */
    private void setEnterKeyAction(AnchorPane root) {
        root.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                sendButton.fire();
                ev.consume();
            }
        });
    }

    /**
     * Class to properly format the messages displayed in a conversation.
     */
    class ConversationCell extends ListCell<Message> {

        @Override
        protected void updateItem(Message message, boolean empty) {
            super.updateItem(message, empty);
            if (message == null) {
                setText(null);
                return;
            }

            Color textColor;
            String text = message.toString();
            TextAlignment alignment;

            if (message.getIsFrom().equals(myUser)) {
                textColor = Color.GREEN;
                alignment = TextAlignment.RIGHT; // TODO : Not working
            } else {
                textColor = Color.BLUE;
                alignment = TextAlignment.LEFT; // TODO : Not working
            }
            setTextAlignment(alignment);
            setTextFill(textColor);
            setText(text);
        }
    }
}
