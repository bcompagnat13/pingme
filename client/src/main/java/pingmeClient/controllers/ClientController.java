package pingmeClient.controllers;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import pingmeClient.ServerConnectionReader;
import pingmeUtil.models.Conversation;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * Controller for the Client view.
 * Main controller.
 */
public class ClientController implements Initializable {

    @FXML
    public AnchorPane clientPane;
    public AnchorPane leftAnchorPane;
    public AnchorPane conversationPane;
    public Label userNameLabel;

    private UsersController serverInfoController;
    private BooleanProperty connected = new SimpleBooleanProperty(false);
    private User myUser;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private SimpleStringProperty username;
    private TreeMap<User, Conversation> conversationMap;
    private ConversationController conversationController;
    private ServerConnectionController serverConnectionController;

    /**
     * Constructor for ClientController
     */
    public ClientController() {
        this.username =new SimpleStringProperty("Connect to server");
        this.conversationMap = new TreeMap<>();
        this.serverConnectionController = new ServerConnectionController(this);
        this.connected.addListener((observable, oldValue, newValue) -> Platform.runLater(
                () -> username.set(String.format("%s (%s)", myUser.getUsername(), newValue? "Connected": "Disconnected"))));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.userNameLabel.textProperty().bind(this.username);
       this.showServerConnectionPane();
    }

    /**
     * Handler to connect to the server.
     * @param name username of the client.
     * @param serverIP String of the server's IP.
     * @param serverPort integer of the server's port.
     */
    public void connect(String name, String serverIP, int serverPort) {
        myUser = new User(name);
        this.conversationController = new ConversationController(myUser, this);
        System.out.println("Initiating connection to : " + serverIP + ":" + serverPort);
        serverInfoController = new UsersController(myUser, this);
        try {
            Socket sock = new Socket(serverIP, serverPort);
            this.connected.set(true);
            outputStream = new ObjectOutputStream(sock.getOutputStream());
            inputStream = new ObjectInputStream(sock.getInputStream());
            outputStream.writeObject(myUser);
            Thread connectionReader = new Thread(new ServerConnectionReader(inputStream, serverInfoController, this));
            connectionReader.start();

            FXMLLoader serverInfoLoader = new FXMLLoader(getClass().getResource("/views/UsersView.fxml"));
            serverInfoLoader.setController(serverInfoController);
            Node serverInfoNode = serverInfoLoader.load();
            leftAnchorPane.getChildren().add(serverInfoNode);
        } catch  (UnknownHostException e) {
            this.connected.set(false);
            System.out.println("couldn't connect");
            this.serverConnectionController.setInfoMessage("Couldn't connect..");
            e.printStackTrace();
        } catch (ConnectException e) {
            this.connected.set(false);
            System.out.println("couldn't connect");
            this.serverConnectionController.setInfoMessage("Couldn't connect..");
        } catch (Exception e) {
            this.connected.set(false);
            e.printStackTrace();
            this.serverConnectionController.setInfoMessage("Couldn't connect..");
        }
    }

    /**
     * Handler for when we get disconnected from the server.
     * Shows the connection pane again.
     */
    public void disconnected() {
        connected.set(false);
        conversationPane.getChildren().clear();
        leftAnchorPane.getChildren().clear();
        showServerConnectionPane();
        this.serverConnectionController.setInfoMessage("Lost connection with server");
    }

    /**
     * Sends a message to the server.
     * @param message Message to send to the server.
     */
    public void sendMessage(Message message) {
        try {
            outputStream.writeObject(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handler when a user is selected.
     * Opens the conversation pane with the related conversation.
     * Creates the conversation if not created yet.
     * @param selectedUser User selected to display the conversation with.
     */
    public void selectUser(User selectedUser) {
        try {
            FXMLLoader messageWindowLoader = new FXMLLoader(getClass().getResource("/views/ConversationView.fxml"));
            messageWindowLoader.setController(this.conversationController);
            Node messageWindowNode = messageWindowLoader.load();
            conversationPane.getChildren().add(messageWindowNode);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Conversation currentConversation;
        if (conversationMap.containsKey(selectedUser)) {
            currentConversation = conversationMap.get(selectedUser);
        } else {
            currentConversation = new Conversation(myUser,selectedUser);
            conversationMap.put(selectedUser, currentConversation);
        }
        conversationController.changeConversation(currentConversation);
    }

    /**
     * Adds a received message to a conversation.
     * Creates the conversation if not created yet.
     * @param message Message received to add to a conversation.
     */
    public void addMessage(Message message) {
        User otherParticipant;
        Conversation conversation;
        if (message.getIsTo().equals(this.myUser)) {
            otherParticipant = message.getIsFrom();
        } else {
            otherParticipant = message.getIsTo();
        }
        if (this.conversationMap.containsKey(otherParticipant)) {
            conversation = this.conversationMap.get(otherParticipant);
        } else {
            conversation = new Conversation(this.myUser, otherParticipant);
            conversationMap.put(otherParticipant, conversation);
        }
        Platform.runLater(() -> conversation.addMessage(message));
    }

    /**
     * Adds a conversation received by the server.
     * Allows receiving old conversations when logging in again.
     * @param conversationReceived Conversation to add.
     */
    public void addConversation(Conversation conversationReceived) {
        User otherParticipant;
        if (conversationReceived.getParticipant1().equals(this.myUser)) {
            otherParticipant = conversationReceived.getParticipant2();
        } else {
            otherParticipant = conversationReceived.getParticipant1();
        }
        if (this.conversationMap.containsKey(otherParticipant)) {
            this.conversationMap.replace(otherParticipant, conversationReceived);
        } else {
            conversationMap.put(otherParticipant, conversationReceived);
        }
        if(this.conversationController != null && this.conversationController.getCurrentConversation() != null &&
                this.conversationController.getCurrentConversation().equals(conversationReceived)) {
            this.conversationController.changeConversation(conversationReceived);
        }
    }

    /**
     * Handler to show the connection pane which allows connecting to a server.
     */
    private void showServerConnectionPane() {
        try {
            FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("/views/ServerConnectionView.fxml"));
            loginLoader.setController(this.serverConnectionController);
            Node loginNode = loginLoader.load();
            leftAnchorPane.getChildren().add(loginNode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
