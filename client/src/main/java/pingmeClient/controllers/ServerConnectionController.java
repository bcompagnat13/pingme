package pingmeClient.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * ServerConnectionView controller.
 */
public class ServerConnectionController implements Initializable {

    @FXML
    public Button connectButton;
    public TextField userNameTextField;
    public TextField serverIpTextField;
    public TextField serverPortTextField;
    public AnchorPane connectAnchorPane;
    public Label infoMessageLabel;

    private ClientController clientController;

    /**
     * Constructor.
     * @param clientController ClientController.
     */
    ServerConnectionController(ClientController clientController) {
        this.clientController = clientController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.connectButton.setOnAction(this::connect);
        setEnterKeyAction(connectAnchorPane);
        this.infoMessageLabel.setText("");
    }

    /**
     * Sets the info message displayed for connection attempt information.
     * @param infoMessage String to display.
     */
    public void setInfoMessage(String infoMessage) {
        this.infoMessageLabel.setText(infoMessage);
    }

    /**
     * Connect button handler to initiate a connection to the server.
     * @param event ActionEvent of the button pressed.
     */
    private void connect(ActionEvent event) {
        // TODO input validation
        int serverPort = Integer.parseInt(serverPortTextField.getText());
        this.clientController.connect(userNameTextField.getText(), serverIpTextField.getText(), serverPort);
    }

    /**
     * Enter key handler to initiate a connection from the keyboard when pressed.
     * @param root AnchorPane to associate the action to.
     */
    private void setEnterKeyAction(AnchorPane root) {
        root.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                connectButton.fire();
                ev.consume();
            }
        });
    }
}
