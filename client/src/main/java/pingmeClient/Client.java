package pingmeClient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import pingmeClient.controllers.ClientController;

import java.io.IOException;

/**
 * Main Client class.
 */
public class Client extends Application {

    @Override
    public void start(Stage stage) {
        try {
            FXMLLoader clientLoader = new FXMLLoader(getClass().getResource("/views/ClientView.fxml"));
            clientLoader.setController(new ClientController());
            Parent rootWindow = clientLoader.load();
            Scene scene = new Scene(rootWindow);
            stage.setOnCloseRequest(
                    e -> {
                        e.consume();
                        System.out.println("Quiting!");
                        System.exit(0);
                    } );
            stage.setTitle("PingMe");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setWidth(815);
            stage.setHeight(640);
            stage.show();
        } catch (IOException e) {
            System.out.println("Exception loading FXML: " + e.getMessage() + "\n" + e);
            e.printStackTrace();
        }
    }

    /**
     * Main to start a client.
     * @param args to start the client.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
