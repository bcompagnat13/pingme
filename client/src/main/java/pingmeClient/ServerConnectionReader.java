package pingmeClient;

import javafx.application.Platform;
import pingmeClient.controllers.ClientController;
import pingmeClient.controllers.UsersController;
import pingmeUtil.models.Conversation;
import pingmeUtil.models.Message;
import pingmeUtil.models.User;

import java.io.ObjectInputStream;
import java.util.TreeSet;

/**
 * Runnable class for reading objects from the server on its own thread.
 */
public class ServerConnectionReader implements Runnable {

    private final UsersController usersController;
    private final ClientController clientController;
    private final ObjectInputStream inputStream;

    /**
     * Constructor.
     * @param inputStream ObjectInputStream from the socket connected to the server.
     * @param usersController UserController for sending new user information to the proper controller.
     * @param clientController ClientController for sending the information to the controller.
     */
    public ServerConnectionReader(ObjectInputStream inputStream, UsersController usersController, ClientController clientController) {
        this.inputStream = inputStream;
        this.usersController = usersController;
        this.clientController = clientController;
    }

    /**
     * Implementation of the run method from the Runnable interface.
     * Waits to receive an object from the server and sends it to the proper controller.
     */
    public void run() {
        Object objectReceived;
        try {
            while ((objectReceived = this.inputStream.readObject()) != null) {
                System.out.println("Received an object from the server : " + objectReceived.getClass());
                if(objectReceived instanceof TreeSet) {
                    this.usersController.addUsers((TreeSet<User>) objectReceived);
                } else if (objectReceived instanceof Message) {
                    Message messageReceived = (Message) objectReceived;
                    this.clientController.addMessage(messageReceived);
                } else if (objectReceived instanceof Conversation) {
                    Conversation conversationReceived = (Conversation) objectReceived;
                    this.clientController.addConversation(conversationReceived);
                }
            }
        } catch (Exception e) {
            Platform.runLater(clientController::disconnected);
            e.printStackTrace();
        }
    }
}
