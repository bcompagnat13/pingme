module pingmeClient {
    requires javafx.controls;
    requires javafx.fxml;
    requires pingmeUtil;

    exports pingmeClient;
    exports pingmeClient.controllers;
}