package pingmeUtil.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class UserTest {
    private final String TEST_USERNAME = "testy";
    private User user;

    @BeforeEach
    public void beforeEach() {
        user = new User(TEST_USERNAME);
    }

    @Test
    public void isOnline_test() {
        Assertions.assertFalse(user.isOnline());
        user.setIsOnline(true);
        Assertions.assertTrue(user.isOnline());
        user.setIsOnline(false);
        Assertions.assertFalse(user.isOnline());
    }

    @Test
    public void toString_test() {
        Assertions.assertEquals(TEST_USERNAME, user.toString());
    }

    @Test
    public void equals_test() {
        User sameUser = new User(TEST_USERNAME);
        User differentUser = new User("testy2");
        Assertions.assertEquals(user, sameUser);
        Assertions.assertNotEquals(user, differentUser);
    }

    @Test
    public void getUsername_test() {
        Assertions.assertEquals(TEST_USERNAME, user.getUsername());
    }

    @Test
    public void compareTo_test() {
        User sameUser = new User(TEST_USERNAME);
        User differentUser = new User("testy2");
        Assertions.assertEquals(0, user.compareTo(sameUser));
        Assertions.assertEquals(1, differentUser.compareTo(user));
        Assertions.assertEquals(-1, user.compareTo(differentUser));
    }
}
