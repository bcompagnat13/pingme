package pingmeUtil.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MessageTest {
    private final String IS_TO_USERNAME = "Potter";
    private final String IS_FROM_USERNAME = "Dumbledore";
    private final String TEST_MESSAGE = "Welcome to  Hogwarts!";
    private final User isToUser = new User(IS_TO_USERNAME);
    private final User isFromUser = new User(IS_FROM_USERNAME);
    private Message normalMessage;
    private Message nullIsFromMessage;
    private Message nullIsToMessage;
    private Message nullMessageMessage;
    private Message emptyMessageMessage;

    @BeforeEach
    public void beforeEach() {
        normalMessage = new Message(TEST_MESSAGE, isFromUser, isToUser);
        nullIsFromMessage = new Message(TEST_MESSAGE, null, isToUser);
        nullIsToMessage = new Message(TEST_MESSAGE, isFromUser, null);
        nullMessageMessage = new Message(null, isFromUser, isToUser);
        emptyMessageMessage = new Message("", isFromUser, isToUser);
    }

    @Test
    public void toString_test() {
        Assertions.assertTrue(normalMessage.toString().matches(
                String.format("\\d\\d:\\d\\d:\\d\\d\\s%s\\s:\\s%s", IS_FROM_USERNAME, TEST_MESSAGE)));

        /* Edge cases */
        Assertions.assertTrue(nullIsFromMessage.toString().matches(
                String.format("\\d\\d:\\d\\d:\\d\\d\\snull\\s:\\s%s", TEST_MESSAGE)));
        Assertions.assertTrue(nullIsToMessage.toString().matches(
                String.format("\\d\\d:\\d\\d:\\d\\d\\s%s\\s:\\s%s", IS_FROM_USERNAME, TEST_MESSAGE)));
        Assertions.assertTrue(nullMessageMessage.toString().matches(
                String.format("\\d\\d:\\d\\d:\\d\\d\\s%s\\s:\\snull", IS_FROM_USERNAME)));
        Assertions.assertTrue(emptyMessageMessage.toString().matches(
                String.format("\\d\\d:\\d\\d:\\d\\d\\s%s\\s:\\s", IS_FROM_USERNAME)));
    }

    @Test
    public void getMessage_test() {
        Assertions.assertEquals(TEST_MESSAGE, normalMessage.getMessage());

        /* Edge cases */
        Assertions.assertNull(nullMessageMessage.getMessage());
        Assertions.assertEquals("", emptyMessageMessage.getMessage());
        Assertions.assertEquals(TEST_MESSAGE, nullIsToMessage.getMessage());
        Assertions.assertEquals(TEST_MESSAGE, nullIsFromMessage.getMessage());
    }

    @Test
    public void getIsFrom_test() {
        Assertions.assertSame(isFromUser, normalMessage.getIsFrom());

        /* Edge cases */
        Assertions.assertNull(nullIsFromMessage.getIsFrom());
        Assertions.assertSame(isFromUser, nullIsToMessage.getIsFrom());
        Assertions.assertSame(isFromUser, nullMessageMessage.getIsFrom());
        Assertions.assertSame(isFromUser, emptyMessageMessage.getIsFrom());
    }

    @Test
    public void getIsTo_test() {
        Assertions.assertSame(isToUser, normalMessage.getIsTo());

        /* Edge cases */
        Assertions.assertNull(nullIsToMessage.getIsTo());
        Assertions.assertSame(isToUser, nullIsFromMessage.getIsTo());
        Assertions.assertSame(isToUser, nullMessageMessage.getIsTo());
        Assertions.assertSame(isToUser, emptyMessageMessage.getIsTo());
    }
}
