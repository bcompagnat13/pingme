package pingmeUtil.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ObservableLimitedListTest {
    private final int LIST_LIMIT = 5;
    private ObservableLimitedList<Integer> list;

    @BeforeEach
    public void beforeEach() {
        list = new ObservableLimitedList<>(LIST_LIMIT);
    }

    @Test
    public void add_test() {
        boolean isAdded = list.add(213);
        Assertions. assertTrue(isAdded);
        Assertions.assertEquals(1, list.size());
        Assertions.assertEquals(213, list.get(0));

        isAdded = list.add(777);
        Assertions. assertTrue(isAdded);
        Assertions.assertEquals(2, list.size());
        Assertions.assertEquals(213, list.get(0));
        Assertions.assertEquals(777, list.get(1));
    }

    @Test
    public void remove_test() {
        list.add(100);
        list.add(200);
        list.add(300);
        list.remove(0);
        Assertions.assertEquals(2, list.size());
        Assertions.assertEquals(200, list.get(0));
        Assertions.assertEquals(300, list.get(1));
    }

    @Test
    public void limit_test() {
        for(int i = 0; i < 10; i++) {
            list.add(i);
        }
        Assertions.assertEquals(LIST_LIMIT, list.size());
        Assertions.assertEquals(5, list.get(0));
    }

    @Test
    void doList_test() {
        list.add(0);
        list.add(1);
        list.doSet(0, 5);
        Assertions.assertEquals(2, list.size());
        Assertions.assertEquals(5, list.get(0));
        Assertions.assertEquals(1, list.get(1));
    }
}
