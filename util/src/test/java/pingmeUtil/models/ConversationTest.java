package pingmeUtil.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConversationTest {
    private final User participant1 = new User("Barney");
    private final User participant2 = new User("Robin");
    private Conversation conversation;
    private Conversation nullParticipant1Conversation;
    private Conversation nullParticipant2Conversation;

    @BeforeEach
    public void beforeEach() {
        conversation = new Conversation(participant1, participant2);
        nullParticipant1Conversation = new Conversation(null, participant2);
        nullParticipant2Conversation = new Conversation(participant1, null);
    }

    @Test
    public void getParticipant1_test() {
        Assertions.assertSame(participant1, conversation.getParticipant1());

        /* Edge cases */
        Assertions.assertSame(participant1, nullParticipant2Conversation.getParticipant1());
        Assertions.assertNull(nullParticipant1Conversation.getParticipant1());
    }

    @Test
    public void getParticipant2_test() {
        Assertions.assertSame(participant2, conversation.getParticipant2());

        /* Edge cases */
        Assertions.assertSame(participant2, nullParticipant1Conversation.getParticipant2());
        Assertions.assertNull(nullParticipant2Conversation.getParticipant2());
    }

    @Test
    public void messages_test() {
        String messageText = "Legend, wait for it, dary!";
        Message goodMessage = new Message(messageText, participant1, participant2);

        boolean isAdded = conversation.addMessage(goodMessage);
        Assertions.assertTrue(isAdded);
        Assertions.assertEquals(1, conversation.getMessages().size());
        Assertions.assertSame(goodMessage, conversation.getMessages().get(0));

        Message nullParticipantMessage = new Message(messageText, participant1, null);
        isAdded = conversation.addMessage(nullParticipantMessage);
        Assertions.assertFalse(isAdded);
        Assertions.assertEquals(1, conversation.getMessages().size());
        Assertions.assertSame(goodMessage, conversation.getMessages().get(0));
    }

    @Test
    public void equals_test() {
        Assertions.assertNotEquals(conversation, participant1);
        Assertions.assertEquals(conversation, conversation);
        Conversation equalConversation = new Conversation(participant1, participant2);
        Assertions.assertEquals(conversation, equalConversation);
        equalConversation = new Conversation(participant2, participant1);
        Assertions.assertEquals(conversation, equalConversation);
        Conversation notEqualConversation = new Conversation(participant1, null);
        Assertions.assertNotEquals(conversation, notEqualConversation);

    }

}
