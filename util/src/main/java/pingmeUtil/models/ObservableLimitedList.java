package pingmeUtil.models;

import javafx.collections.ModifiableObservableListBase;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * Implementation of an Observable List where the list has a limit on a FIFO concept.
 * @param <T> type of the object contained in the list.
 */
public class ObservableLimitedList<T> extends ModifiableObservableListBase<T> implements Serializable {
    private LinkedList<T> list;
    private final int maxSize;

    /**
     * Constructor of the list.
     * @param maxSize maximum number of items the list can contain.
     */
    public ObservableLimitedList(int maxSize) {
        this.maxSize = maxSize;
        list = new LinkedList<>();
    }

    @Override
    public boolean add(T element) {
        boolean result = super.add(element);
        if (size() > maxSize) {
            remove(0);
        }
        return result;
    }

    @Override
    public T get(int index) {
        return list.get(index);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    protected void doAdd(int index, T element) {
        list.add(index, element);
    }

    @Override
    protected T doSet(int index, T element) {
        return list.set(index, element);
    }

    @Override
    protected T doRemove(int index) {
        return list.remove(index);
    }
}