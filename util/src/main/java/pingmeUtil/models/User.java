package pingmeUtil.models;

import java.io.Serializable;

/**
 * Class for containing the information about a user.
 */
public class User implements Serializable, Comparable {
    private String username;
    private boolean isOnline;

    /**
     * Constructor of the user.
     * @param username for authentication.
     */
    public User(String username) {
        this.username = username;
        this.isOnline = false;
    }

    @Override
    public String toString() {
        return this.username;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof User && ((User) o).getUsername().equals(this.username);
    }

    @Override
    public int compareTo(Object o) {
        return this.getUsername().compareTo(((User) o).getUsername());
    }

    /**
     * Setter for online status.
     * @param isOnline online status, false if offline.
     */
    public void setIsOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    /**
     * Getter for online status.
     * @return true if online.
     */
    public boolean isOnline(){
        return this.isOnline;
    }

    /**
     * Getter for the username.
     * @return String of the username.
     */
    public String getUsername() {
        return username;
    }
}
