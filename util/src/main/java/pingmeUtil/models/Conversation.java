package pingmeUtil.models;

import java.io.Serializable;

/**
 * Conversation class keeping the history of 2 users discussion.
 */
public class Conversation implements Serializable {
    private final int HISTORY_LIMIT = 100;

    private final User participant1;
    private final User participant2;
    private final ObservableLimitedList<Message> messages;

    public Conversation(User participant1, User participant2) {
        this.participant1 = participant1;
        this.participant2 = participant2;
        messages = new ObservableLimitedList<>(HISTORY_LIMIT);
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Conversation)) {
            return false;
        }
        Conversation conversation = (Conversation) o;
        if(conversation.getParticipant1() == null || conversation.getParticipant2() == null) {
            return false;
        }
        return (conversation.getParticipant1().equals(this.participant1) &&
                conversation.getParticipant2().equals(this.participant2))
         ||
                (conversation.getParticipant1().equals(this.participant2) &&
                conversation.getParticipant2().equals(this.participant1));
    }

    /**
     * Getter for the participant 1 of 2.
     * @return User participating in the conversation.
     */
    public User getParticipant1() {
        return participant1;
    }

    /**
     * Getter for the participant 2 of 2.
     * @return User participating in the conversation.
     */
    public User getParticipant2() {
        return participant2;
    }

    /**
     * Getter for the list of messages between the two users.
     * @return ObservableLimitedList of messages part of the conversation.
     */
    public ObservableLimitedList<Message> getMessages() {
        return messages;
    }

    /**
     * Add a message to the discussion.
     * The message shall concern the same users as this conversation.
     * @param message in between two users part of the conversation.
     * @return true if the message is valid and added.
     */
    public boolean addMessage(Message message) {
        if(!validateParticipants(message)) {
            return false;
        }
        this.messages.add(message);
        return true;
    }

    /**
     * Validates that a message contains the same participants as the message received.
     * @param message to validate.
     * @return true if the message contains same users as the conversation.
     */
    private boolean validateParticipants(Message message) {
        if(message.getIsTo() == null || message.getIsFrom() == null) {
            return false;
        }
        return (message.getIsTo().equals(this.participant1) && message.getIsFrom().equals(this.participant2)) ||
                (message.getIsFrom().equals(this.participant1) && message.getIsTo().equals(this.participant2));
    }
}