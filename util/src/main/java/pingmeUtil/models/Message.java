package pingmeUtil.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class for messages between users.
 */
public class Message implements Serializable {
    private String message;
    private String timestamp;
    private User isTo;
    private  User isFrom;

    /**
     * Constructor for the Message object.
     * @param message String containing the message.
     * @param isFrom User initiating the message to another user.
     * @param isTo User that receives the message from another user.
     */
    public Message(String message, User isFrom, User isTo) {
        this.message = message;
        this.isFrom = isFrom;
        this.isTo = isTo;
        this.timestamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    @Override
    public String toString() {
        return String.format("%s %s : %s",this.timestamp, this.isFrom, this.message);
    }

    /**
     * Getter for the content of the message.
     * @return String of the message.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Getter for the originator of the message.
     * @return User originating the message.
     */
    public User getIsFrom() {
        return isFrom;
    }

    /**
     * Getter for the destination user of the message.
     * @return User receiving the message.
     */
    public User getIsTo() {
        return isTo;
    }
}
