package pingmeUtil;

// TODO: Make a configuration file instead.
public final class Constants {
    public static final int SERVER_PORT = 9000;

    /**
     * Private constructor as this class is dedicated to static constants.
     */
    private Constants() {}
}